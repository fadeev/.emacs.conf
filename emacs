;; ~/.emacs
;; (load "~/.emacs.conf/emacs")

(require 'org-id)
(require 'package)

(setq default-input-method "russian-computer"
      make-backup-files nil
      auto-save-default nil
      org-completion-use-ido t
      inhibit-startup-screen t
      ido-enable-flex-matching t
      ido-everywhere t
      ido-auto-merge-work-directories-length -1
      indent-tabsmode nil
      scroll-conservatively 10000
      mouse-wheel-scroll-amount '(1 ((shift) . 1))
      mouse-wheel-progressive-speed nil
      scroll-step 1
      inferior-lisp-program "racket")

(menu-bar-mode 0)
(scroll-bar-mode 0)
(if tool-bar-mode (tool-bar-mode 0))
(fringe-mode 0)
(toggle-frame-fullscreen)
(blink-cursor-mode 0)
(show-paren-mode t)
(ido-mode t)
(set-default 'truncate-lines 1)
(savehist-mode)
(add-hook 'after-init-hook #'neotree-toggle)
(set-face-attribute 'mode-line nil :box nil)
(set-face-attribute 'mode-line-inactive nil :box nil)
(load-theme 'wombat t)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(defun epg--list-keys-1 (context name mode)
  (let ((args (append (if (epg-context-home-directory context)
			  (list "--homedir"
				(epg-context-home-directory context)))
		      '("--with-colons" "--no-greeting" "--batch"
			"--with-fingerprint" "--with-fingerprint")
		      (unless (eq (epg-context-protocol context) 'CMS)
			'("--fixed-list-mode"))))
	(list-keys-option (if (memq mode '(t secret))
			      "--list-secret-keys"
			    (if (memq mode '(nil public))
				"--list-keys"
			      "--list-sigs")))
	(coding-system-for-read 'binary)
	keys string field index)
    (if name
	(progn
	  (unless (listp name)
	    (setq name (list name)))
	  (while name
	    (setq args (append args (list list-keys-option (car name)))
		  name (cdr name))))
      (setq args (append args (list list-keys-option))))
    (with-temp-buffer
      (apply #'call-process
	     (epg-context-program context)
	     nil (list t nil) nil args)
      (goto-char (point-min))
      (while (re-search-forward "^[a-z][a-z][a-z]:.*" nil t)
	(setq keys (cons (make-vector 15 nil) keys)
	      string (match-string 0)
	      index 0
	      field 0)
	(while (and (< field (length (car keys)))
		    (eq index
			(string-match "\\([^:]+\\)?:" string index)))
	  (setq index (match-end 0))
	  (aset (car keys) field (match-string 1 string))
	  (setq field (1+ field))))
      (nreverse keys))))
